export type Image = {
  id: string;
  label: string;
  url: string;
  width: number;
  height: number;
};

// URL and file can not be set both at the same time
export type ImageUpload = Exclude<
  { label?: string; url?: string; file?: File },
  { label?: string; url: string; file: File }
>;

export type ImagePreview = {
  name: string;
  url: string;
};
