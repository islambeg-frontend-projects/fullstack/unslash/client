import { defineStore } from "pinia";
import type { Image, ImageUpload } from "@/models/image";
import * as api from "@/services/api";

export const useGallery = defineStore("gallery", {
  state: () => ({
    images: <Image[]>[],
    searchedForImages: <Image[]>[],
    isFetchedAll: false,
    isFetchedAllForFilter: false,
    upload: <Upload>{
      status: "none",
    },
    loading: <"done" | "going" | "error">"done",
    labelFilter: "",
  }),
  getters: {
    uploadStatus: (state) => state.upload.status,
    uploadPayload: (state) => state.upload.payload,
    uploadResult: (state) => state.upload.result,
    error: (state) => state.upload.error,
    filteredImages: (state) =>
      state.labelFilter
        ? state.images
            .concat(state.searchedForImages)
            .filter((i) =>
              i.label
                .toLocaleLowerCase()
                .includes(state.labelFilter.toLocaleLowerCase())
            )
        : state.images,
  },

  actions: {
    getNextImages(lastId = "", count = 10) {
      this.loading = "going";
      return api
        .getImages({ lastId, count, labelFilter: this.labelFilter })
        .then((res) => {
          if (res instanceof Error) {
            this.loading = "error";
            return;
          }

          this.$patch((state) => {
            if (state.labelFilter) {
              state.isFetchedAllForFilter = res.isFetchedAll;
              state.searchedForImages = res.images;
            } else {
              state.isFetchedAll = res.isFetchedAll;
              state.images = state.images.concat(res.images);
            }
            state.loading = "done";
          });
        });
    },

    deleteImage(imageId: string) {
      return api.deleteImage(imageId).then((res) => {
        if (res instanceof api.ServerError) {
          return;
        }

        this.images = this.images.filter((i) => i.id !== imageId);
      });
    },

    uploadImage(payload: ImageUpload) {
      this.upload = {
        payload: payload,
        status: "uploading",
      };
      api.uploadImage(payload).then((res) => {
        if (res instanceof Error) {
          this.$patch({
            upload: {
              status: "error",
              error: {
                statusCode: res.statusCode,
                message: res.message,
              },
            },
          });
          return;
        }

        this.$patch((state) => {
          state.upload = {
            payload,
            status: "success",
            result: res,
          };
          state.images = [res, ...state.images];
        });
      });
    },

    resetUpload() {
      this.upload = {
        status: "none",
      };
    },

    prepareUploadRetry() {
      this.upload.status = "retry";
    },
  },
});

interface Upload {
  payload?: ImageUpload;
  status: "none" | "uploading" | "success" | "error" | "retry";
  result?: Image;
  error?: {
    statusCode: number;
    message: string;
  };
}
