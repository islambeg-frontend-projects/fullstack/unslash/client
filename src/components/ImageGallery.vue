<script setup lang="ts">
import { ref, computed, onMounted } from "vue";
import type { Ref } from "vue";
import {
  useWindowSize,
  useElementSize,
  useCssVar,
  watchPausable,
  until,
  useEventListener,
  useThrottleFn,
} from "@vueuse/core";
import { useGallery } from "@/stores/gallery";
import {
  getIdForImage,
  focusElementById,
  getRowSpan,
  getRootFontSize,
} from "@/utils/dom";
import BaseModal from "@/components/BaseModal.vue";
import ImageSlider from "@/components/ImageSlider.vue";
import ImageTile from "@/components/ImageTile.vue";
import LoadingSpinner from "@/components/LoadingSpinner.vue";
import DeleteImageConfirmation from "@/components/DeleteImageConfirmation.vue";
import TransitionElementFade from "@/components/TransitionElementFade.vue";
import TransitionGroupFade from "@/components/TransitionGroupFade.vue";

const galleryStore = useGallery();
const galleryEl: Ref<HTMLElement | null> = ref(null);

const { width: windowWidth, height: windowHeight } = useWindowSize();
const { height: galleryHeight } = useElementSize(galleryEl);
const semiSmallScreenWidth = computed(() => {
  const propName = "--semi-small-screen__width";
  const propEmValue = useCssVar(propName).value.slice(0, -2);
  const pxValue = Number(propEmValue) * 16;
  return pxValue;
});

const { stop: stopFetchingNewImages } = watchPausable(galleryHeight, () => {
  if (
    galleryHeight.value > windowHeight.value ||
    galleryStore.isFetchedAllForFilter
  ) {
    return;
  }

  const lastImageIdShown = galleryStore.filteredImages.length
    ? galleryStore.filteredImages[galleryStore.filteredImages.length - 1].id
    : "";
  galleryStore.getNextImages(lastImageIdShown, 10);
});
onMounted(() => {
  galleryStore.getNextImages();
  until(() => galleryStore.isFetchedAll)
    .toBeTruthy()
    .then(stopFetchingNewImages);
});

/*
  manipulations with slider
*/
const sliderWidth = computed(() =>
  windowWidth.value > semiSmallScreenWidth.value ? "90vw" : "95vw"
);
const sliderHeight = computed(() =>
  windowWidth.value > semiSmallScreenWidth.value ? "95vh" : "85vh"
);
const showSlider = ref(false);
const sliderImageArrayIdx = ref(0);
function handleImageSlideView(imageArrayIdx: number) {
  sliderImageArrayIdx.value = imageArrayIdx;
  showSlider.value = true;
}
function handleSliderClosing() {
  const imageId = galleryStore.images[sliderImageArrayIdx.value].id;
  focusElementById(getIdForImage(imageId));
  showSlider.value = false;
}

/*
  manipulations with image deletion
*/
const imageIdForDeletion: Ref<string | null> = ref(null);
const showDeletionConfirmation = ref(false);
function handleDeletionRequest(imageId: string) {
  imageIdForDeletion.value = imageId;
  showDeletionConfirmation.value = true;
}
function handleDeletionClosing(action?: "cancel" | "confirm") {
  if (imageIdForDeletion.value) {
    if (action == "cancel") {
      focusElementById(getIdForImage(imageIdForDeletion.value));
    } else if (action == "confirm") {
      galleryStore.deleteImage(imageIdForDeletion.value);
    }
  }

  imageIdForDeletion.value = null;
  showDeletionConfirmation.value = false;
}

/*
  manipulations with window resizing and scrolling
*/
const rootFontSizePx = ref(10);
const gridRowSize = computed(() => rootFontSizePx.value * 5);
const handleWindowResize = useThrottleFn(
  () => (rootFontSizePx.value = getRootFontSize()),
  300
);

const scrollLocked = ref(false);
function handleScroll() {
  if (galleryStore.isFetchedAll) {
    window.removeEventListener("scroll", handleScroll);
    return;
  }

  if (!galleryEl.value || scrollLocked.value) {
    return;
  }

  const galleryHeight = galleryEl.value.scrollHeight;
  const scrollHeight = window.scrollY + windowHeight.value;
  const scrollPrefetchMargin = 25 * rootFontSizePx.value;
  if (scrollHeight + scrollPrefetchMargin > galleryHeight) {
    scrollLocked.value = true;
    const lastImageIdShown =
      galleryStore.filteredImages[galleryStore.filteredImages.length - 1].id;
    galleryStore
      .getNextImages(lastImageIdShown, 10)
      .finally(() => (scrollLocked.value = false));
  }
}

useEventListener(window, "resize", handleWindowResize);
useEventListener(window, "scroll", handleScroll);
</script>

<template>
  <BaseModal
    v-if="showSlider"
    :styles="{ width: sliderWidth, backgroundColor: 'transparent' }"
    @close-modal="handleSliderClosing"
  >
    <ImageSlider
      v-model:current-idx="sliderImageArrayIdx"
      :width="sliderWidth"
      :height="sliderHeight"
    />
  </BaseModal>

  <BaseModal
    v-if="showDeletionConfirmation"
    @close-modal="handleDeletionClosing"
  >
    <DeleteImageConfirmation
      @cancel="() => handleDeletionClosing('cancel')"
      @confirm="handleDeletionClosing('confirm')"
    />
  </BaseModal>

  <TransitionElementFade>
    <section v-if="galleryStore.images.length" class="gallery" ref="galleryEl">
      <!--
      we create div because it is not possible to use ::before and ::after for <img>
    -->
      <TransitionGroupFade>
        <ImageTile
          v-for="(image, idx) in galleryStore.filteredImages"
          :key="image.id"
          :image="image"
          :style="{
            'grid-row-end': `span ${getRowSpan(image.height, gridRowSize)}`,
          }"
          @slide-view="() => handleImageSlideView(idx)"
          @deletion="handleDeletionRequest"
        />
      </TransitionGroupFade>
    </section>
  </TransitionElementFade>

  <TransitionElementFade>
    <div v-if="galleryStore.loading === 'going'" class="loading">
      <LoadingSpinner class="spinner" aria-label="Loading images" />
    </div>
  </TransitionElementFade>

  <TransitionElementFade>
    <p v-if="galleryStore.loading === 'error'" class="error">
      Something went wrong.
    </p>
  </TransitionElementFade>
</template>

<style lang="scss" scoped>
@use "@/assets/styles/bootstrap";

.gallery {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
  grid-auto-rows: 0;
  gap: 4.5rem;
  margin-bottom: 3rem;

  @include bootstrap.respond-to-semi-small-screen {
    gap: 2.5rem;
  }

  @include bootstrap.respond-to-tiny-screen {
    gap: 1.5rem;
    grid-template-columns: repeat(auto-fit, minmax(25rem, 1fr));
  }
}

.error {
  font-size: 3.2rem;
  color: var(--color-alert);
}

.loading {
  display: flex;
  justify-content: center;
}

.spinner {
  font-size: 3.6rem;
  width: 5rem;
  height: 5rem;
}
</style>
