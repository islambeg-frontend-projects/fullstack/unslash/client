import { useWindowSize, watchThrottled } from "@vueuse/core";

export function getIdForImage(imageId: number | string) {
  return `gallery-tile_image_${imageId}`;
}

export function getRowSpan(height: number, rowSize: number) {
  const MAX_ROWS_SPAN = 15;
  const MIN_ROWS_SPAN = 3;

  const rowsFitted = Math.floor(height / rowSize);

  if (rowsFitted > MAX_ROWS_SPAN) {
    return MAX_ROWS_SPAN;
  }

  if (rowsFitted < MIN_ROWS_SPAN) {
    return MIN_ROWS_SPAN;
  }

  return rowsFitted;
}

export function getFocusableChildren(parentEl: HTMLElement) {
  return parentEl.querySelectorAll(
    'button:not([disabled]):not([tabindex="-1"]), [href]:not([tabindex="-1"]), input:not([disabled]):not([tabindex="-1"]), select:not([disabled]):not([tabindex="-1"]), textarea:not([disabled]):not([tabindex="-1"]), [tabindex]:not([tabindex="-1"]):not([disabled]), details:not([disabled]):not([tabindex="-1"]), summary:not(:disabled):not([tabindex="-1"])'
  );
}

export function focusElementById(id: string) {
  document.getElementById(id)?.focus();
}

function isKeyPressed(e: KeyboardEvent, key: string) {
  const keyToCheck = key.toLowerCase();
  return e.key.toLowerCase() === keyToCheck || e.code === keyToCheck;
}

export function isTabPressed(e: KeyboardEvent) {
  return isKeyPressed(e, "tab");
}

export function isEscPressed(e: KeyboardEvent) {
  return isKeyPressed(e, "escape");
}

export function isEnterPressed(e: KeyboardEvent) {
  return isKeyPressed(e, "enter");
}

export function isSpacebarPressed(e: KeyboardEvent) {
  return (
    isKeyPressed(e, " ") ||
    isKeyPressed(e, "space") ||
    isKeyPressed(e, "spacebar")
  );
}

export function isImage(file: File | undefined | null) {
  if (!file) {
    return false;
  }

  return Boolean(file.type.match(/^image.*/));
}

export function isValidUrl(url: string | undefined | null) {
  if (!url) {
    return false;
  }

  try {
    new URL(url);

    return true;
  } catch (e) {
    if (!(e instanceof TypeError)) {
      throw e;
    }

    return false;
  }
}

export function getRootFontSize() {
  return Number(
    getComputedStyle(document.documentElement)
      .getPropertyValue("font-size")
      .slice(0, 2)
  );
}

export function makeFontSizeResponsive() {
  const rootEl = document.documentElement;
  const { width: currentWidth } = useWindowSize();
  const baseScreenReferenceWidth = 1440;

  function onChangedWidth(width: number) {
    const fontChangeRate = getFontChangeRate(width);
    const fontBaseSizeMultiplier = Math.pow(
      width / baseScreenReferenceWidth,
      fontChangeRate
    );
    const newFontBaseSize = fontBaseSizeMultiplier * 62.5;
    rootEl.style.fontSize = `${newFontBaseSize.toFixed(4)}%`;
  }

  onChangedWidth(currentWidth.value);

  watchThrottled(currentWidth, onChangedWidth, { throttle: 300 });

  function getFontChangeRate(currentWidth: number) {
    return currentWidth <= baseScreenReferenceWidth ? 0.15 : 1.15;
  }
}
