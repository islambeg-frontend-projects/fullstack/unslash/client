import axios, { AxiosError } from "axios";
import type { Image, ImageUpload } from "@/models/image";

type ServerErrorData = { message?: string };

type AxiosServerError = AxiosError<ServerErrorData>;

export class ServerError extends Error {
  message: string;
  statusCode: number;

  constructor(message?: string, statusCode?: number) {
    super(message);
    this.message = message || "Something went wrong.";
    this.statusCode = statusCode || 500;
  }
}

type Result<T> = Promise<T | ServerError>;

const axiosClient = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
});

export function getImages({
  lastId = "",
  count = 10,
  labelFilter = "",
}): Result<{
  images: Image[];
  isFetchedAll: boolean;
}> {
  return axiosClient
    .get(
      `/image?lastId=${lastId}&count=${count}&labelFilter=${labelFilter}&before=true`
    )
    .then((res) => res.data)
    .catch(mapAxiosError);
}

export function deleteImage(id: string) {
  return axiosClient.delete(`/image/${id}`).catch(mapAxiosError);
}

export function uploadImage(imageUpload: ImageUpload) {
  const { label, url, file } = imageUpload;

  if (!url && !file) {
    throw new Error("Either url or file must be provided.");
  }

  const payload = new FormData();
  payload.append("label", label || "");
  payload.append("url", url || "");
  payload.append("file", file || "");

  return axiosClient
    .post<Image>(`/image`, payload, {
      headers: { "Content-Type": "multipart/form-data" },
    })
    .then((res) => res.data)
    .catch(mapAxiosError);
}

function mapAxiosError(error: AxiosServerError): ServerError {
  return new ServerError(error.response?.data.message, error.response?.status);
}
