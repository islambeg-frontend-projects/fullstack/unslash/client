import { createApp } from "vue";
import { createPinia } from "pinia";
import { makeFontSizeResponsive } from "@/utils/dom";
import App from "@/App.vue";

makeFontSizeResponsive();

const app = createApp(App);

const pinia = createPinia();
app.use(pinia);

app.mount("#app");
